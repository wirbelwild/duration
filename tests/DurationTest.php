<?php

/**
 * Bit&Black Duration. Handling durations in an object-oriented way.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Duration\Tests;

use BitAndBlack\Duration\Duration;
use Generator;
use PHPUnit\Framework\TestCase;

class DurationTest extends TestCase
{
    /**
     * @dataProvider getCanCreateSecondsData
     * @return void
     */
    public function testCanCreateSeconds(Duration $duration, float $secondsExpected): void
    {
        self::assertSame(
            $secondsExpected,
            $duration->getSeconds()
        );
    }

    public function getCanCreateSecondsData(): Generator
    {
        yield [
            Duration::createFromNanoSeconds(1_000_000_000),
            1,
        ];

        yield [
            Duration::createFromMicroSeconds(1_000_000),
            1,
        ];

        yield [
            Duration::createFromMilliSeconds(1_000),
            1,
        ];

        yield [
            Duration::createFromSeconds(60),
            60,
        ];

        yield [
            Duration::createFromMinutes(60),
            3600,
        ];

        yield [
            Duration::createFromHours(24),
            86_400,
        ];

        yield [
            Duration::createFromDays(7),
            604_800,
        ];

        yield [
            Duration::createFromWeeks(52),
            31_449_600,
        ];
    }

    /**
     * @dataProvider timeConversionDataProvider
     */
    public function testCanConvertTime(
        float $hours,
        float $expectedNanoSeconds,
        float $expectedMicroSeconds,
        float $expectedMilliseconds,
        float $expectedSeconds,
        float $expectedMinutes,
        float $expectedHours,
        float $expectedDays,
        float $expectedWeeks,
    ): void {
        $time = Duration::createFromHours($hours);

        self::assertSame($expectedNanoSeconds, $time->getNanoSeconds());
        self::assertSame($expectedMicroSeconds, $time->getMicroSeconds());
        self::assertSame($expectedMilliseconds, $time->getMilliseconds());
        self::assertSame($expectedSeconds, $time->getSeconds());
        self::assertSame($expectedMinutes, $time->getMinutes());
        self::assertSame($expectedHours, $time->getHours());
        self::assertSame($expectedDays, $time->getDays());
        self::assertSame($expectedWeeks, $time->getWeeks());
    }

    public function timeConversionDataProvider(): Generator
    {
        yield [
            1,
            3_600_000_000_000.0,
            3_600_000_000.0,
            3_600_000.0,
            3_600.0,
            60.0,
            1.0,
            0.041666666666666664,
            0.005952380952380952,
        ];

        yield [
            24,
            86_400_000_000_000.0,
            86_400_000_000.0,
            86_400_000.0,
            86_400.0,
            1_440.0,
            24.0,
            1.0,
            0.14285714285714285,
        ];
    }
}
