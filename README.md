[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/duration)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/8ea127725b3d4a84a47bbd83445c9a63)](https://www.codacy.com/bb/wirbelwild/duration/dashboard)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/duration/v/stable)](https://packagist.org/packages/bitandblack/duration)
[![Total Downloads](https://poser.pugx.org/bitandblack/duration/downloads)](https://packagist.org/packages/bitandblack/duration)
[![License](https://poser.pugx.org/bitandblack/duration/license)](https://packagist.org/packages/bitandblack/duration)

# Bit&Black Duration

Handling durations in an object-oriented way.

## Installation

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/duration). Add it to your project by running `composer require bitandblack/duration`.

## Usage

Create a new instance like that:

````php
<?php

use BitAndBlack\Duration\Duration;

$duration = Duration::createFromDays(7);
````

You can use

*   `createFromNanoSeconds` to create a duration from nanoseconds.
*   `createFromMicroSeconds` to create a duration from microseconds.
*   `createFromMilliSeconds` to create a duration from milliseconds.
*   `createFromSeconds` to create a duration from seconds.
*   `createFromMinutes` to create a duration from minutes.
*   `createFromHours` to create a duration from hours.
*   `createFromDays` to create a duration from days.
*   `createFromWeeks` to create a duration from weeks.

Get the duration in another unit:

````php
<?php

// This will result in `604800.0`.
$seconds = $duration->getSeconds();
````

You can use

*   `getNanoSeconds` to get the duration as nanoseconds.
*   `getMicroSeconds` to get the duration as microseconds.
*   `getMilliSeconds` to get the duration as milliseconds.
*   `getSeconds` to get the duration as seconds.
*   `getMinutes` to get the duration as minutes.
*   `getHours` to get the duration as hours.
*   `getDays` to get the duration as days.
*   `getWeeks` to get the duration as weeks.

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).