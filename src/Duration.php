<?php

/**
 * Bit&Black Duration. Handling durations in an object-oriented way.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Duration;

class Duration
{
    /**
     * Returns a new instance based on seconds.
     */
    public function __construct(
        private float $seconds
    ) {
    }

    /**
     * Returns a new instance based on nanoseconds.
     */
    public static function createFromNanoSeconds(float $nanoseconds): self
    {
        return self::createFromMicroSeconds($nanoseconds / 1_000);
    }

    /**
     * Returns a new instance based on microseconds.
     */
    public static function createFromMicroSeconds(float $microseconds): self
    {
        return self::createFromMilliSeconds($microseconds / 1_000);
    }

    /**
     * Returns a new instance based on milliseconds.
     */
    public static function createFromMilliSeconds(float $milliseconds): self
    {
        return self::createFromSeconds($milliseconds / 1_000);
    }

    /**
     * Returns a new instance based on seconds.
     */
    public static function createFromSeconds(float $seconds): self
    {
        return new self($seconds);
    }

    /**
     * Returns a new instance based on minutes.
     */
    public static function createFromMinutes(float $minutes): self
    {
        return self::createFromSeconds($minutes * 60);
    }

    /**
     * Returns a new instance based on hours.
     */
    public static function createFromHours(float $hours): self
    {
        return self::createFromMinutes($hours * 60);
    }

    /**
     * Returns a new instance based on days.
     */
    public static function createFromDays(float $days): self
    {
        return self::createFromHours($days * 24);
    }

    /**
     * Returns a new instance based on weeks.
     */
    public static function createFromWeeks(float $weeks): self
    {
        return self::createFromDays($weeks * 7);
    }

    /**
     * Returns the time in nanoseconds.
     */
    public function getNanoSeconds(): float
    {
        return $this->getMicroSeconds() * 1_000;
    }

    /**
     * Returns the time in nanoseconds.
     */
    public function getMicroSeconds(): float
    {
        return $this->getMilliseconds() * 1_000;
    }

    /**
     * Returns the time in milliseconds.
     */
    public function getMilliseconds(): float
    {
        return $this->getSeconds() * 1_000;
    }

    /**
     * Returns the time in seconds.
     */
    public function getSeconds(): float
    {
        return $this->seconds;
    }

    /**
     * Returns the time in minutes.
     */
    public function getMinutes(): float
    {
        return $this->getSeconds() / 60;
    }

    /**
     * Returns the time in hours.
     */
    public function getHours(): float
    {
        return $this->getMinutes() / 60;
    }

    /**
     * Returns the time in days.
     */
    public function getDays(): float
    {
        return $this->getHours() / 24;
    }

    /**
     * Returns the time in weeks.
     */
    public function getWeeks(): float
    {
        return $this->getDays() / 7;
    }
}
